# Prueba

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.22.

# Despliegue Aplicativo

Para realizar el despliegue:

1. clonar el el repositorio usando el comando:
git clone https://gitlab.com/AndresCode11/prueba-tecnica-angular.git.

2. ingresear a la carpeta usando el comando:
cd prueba-tecnica-angular.

3. Instalar dependencias usando el comando:
npm install

4. Para probar en entorno de desarollo:
ng serve

5. Para desplegar aplicativo a servidor (Ejemplo: Apache, IIS, Nginx)
ng build 

el cual nos genera la carpeta de con los archivos java script y html incluyendo 
el inde.html
