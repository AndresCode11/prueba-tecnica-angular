import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateItemComponent } from './pages/create-item/create-item.component';
import { DeleteItemComponent } from './pages/delete-item/delete-item.component';
import { EditItemComponent } from './pages/edit-item/edit-item.component';
import { ViewItemComponent } from './pages/view-item/view-item.component';

const routes: Routes = [
    {
      path: 'create',
      component: CreateItemComponent
    },
    {
      path: 'delete',
      component: DeleteItemComponent
    },
    {
      path: 'edit',
      component: EditItemComponent
    },
    {
      path: 'viewlist',
      component: ViewItemComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
