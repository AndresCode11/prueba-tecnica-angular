import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

    menuItems = [
      {
        path: 'viewlist',
        title: 'View List'
      },
      {
        path: 'create',
        title: 'Create Item'
      },
      {
        path: 'edit',
        title: 'Edit Item'
      },
      {
        path: 'delete',
        title: 'Delete Item'
      },
    ];

    constructor() { }

    ngOnInit() {
    }

}
