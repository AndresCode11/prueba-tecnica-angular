import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ItemServicesService {

  readonly baseUrl = 'http://159.203.184.170:8000/api/item';

  private getAllUrl = this.baseUrl + '/showall';
  private createItemUrl = this.baseUrl + '/create';
  private deleteItemUrl = this.baseUrl + '/delete';
  private editItemUrl = this.baseUrl + '/edit';

  constructor(private httpClient: HttpClient) { }

  public getAllService(): Observable<Object> {
      return this.httpClient.get( this.getAllUrl, {responseType: 'json'} );
  }

  public createItemService(formData: FormData): Observable<Object> {
      return this.httpClient.post( this.createItemUrl, formData, {responseType: 'json'} );
  }

  public deleteItemService(formData: FormData): Observable<Object> {
      return this.httpClient.post( this.deleteItemUrl, formData, {responseType: 'json'} );
  }

  public editItemService(formData: FormData):Observable<Object> {
    return this.httpClient.post( this.editItemUrl, formData, {responseType: 'json'});
  }
}
