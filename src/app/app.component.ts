import { Component } from '@angular/core';
import { ItemServicesService} from './services/item-services.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'prueba';

  constructor(private itemService: ItemServicesService) {
      this.showALL();
  }

  public showALL(){
      this.itemService.getAllService()
      .subscribe(
          (res) => {}
      );
  }
}
