import { Component, OnInit } from '@angular/core';
import { ItemServicesService } from 'src/app/services/item-services.service';
import { TouchSequence } from 'selenium-webdriver';

@Component({
  selector: 'app-view-item',
  templateUrl: './view-item.component.html',
  styleUrls: ['./view-item.component.scss']
})
export class ViewItemComponent implements OnInit {

  public data;

  constructor(private itemService: ItemServicesService) { }

  ngOnInit() {
      this.getAll();
  }

  getAll() {
    this.itemService.getAllService()
    .subscribe(
        (res) => { 
            this.data = res; 
          },
        (error) => { alert('error al obtener');}
    )
  }

}
