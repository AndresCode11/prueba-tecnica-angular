import { Component, OnInit } from '@angular/core';
import { ItemServicesService } from 'src/app/services/item-services.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.scss']
})
export class EditItemComponent implements OnInit {

  public currentItem;
  public data = [];
  public editForm: FormGroup;

  constructor(private itemService: ItemServicesService,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getAll();
    this.createForms();
  }

  getAll() {
      this.itemService.getAllService()
      .subscribe(
          (res: any) => { this.data = res; },
          (error) => { alert('error al obtener');}
      );
  }

  getCurrentItem(index: number) {
      this.formCtrls.id.setValue(this.data[index].id)
      this.formCtrls.title.setValue(this.data[index].tittle);
  }

  get formCtrls() {
      return this.editForm.controls;
  }

  createForms() {
      this.editForm = this.formBuilder.group({
        id: ['',Validators.required],
        title: ['', Validators.required]
      });
  }

  editItem() {
      let formData: FormData = new FormData();
      formData.append('itemId', this.formCtrls.id.value);
      formData.append('title', this.formCtrls.title.value);

      this.itemService.editItemService(formData)
      .subscribe(
          (res) => { 
              alert('Item modificado exitosamente');
              this.editForm.reset(); 
              this.getAll();
            },
          (error) => { this.editForm.reset(); }
      )

  }

}
