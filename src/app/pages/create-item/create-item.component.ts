import { Component, OnInit } from '@angular/core';
import { ItemServicesService } from 'src/app/services/item-services.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-item',
  templateUrl: './create-item.component.html',
  styleUrls: ['./create-item.component.scss']
})
export class CreateItemComponent implements OnInit {

  public createForm: FormGroup;

  constructor(private itemService: ItemServicesService,
              private formBuilder: FormBuilder) { }

  ngOnInit() { 
    this.createForms();
  }

  public createItem() {
      let formData: FormData = new FormData;
      formData.append('tittle', this.formCtrls.title.value);
      this.itemService.createItemService(formData)
      .subscribe(
          (res) => { 
              alert('item creado con exito');
              this.createForm.reset();
          },
          (error) => {}
      )
  }

  get formCtrls() {
      return this.createForm.controls;
  }

  createForms() {
    this.createForm = this.formBuilder.group({
      title: ['', Validators.required]
    });
  }

}
