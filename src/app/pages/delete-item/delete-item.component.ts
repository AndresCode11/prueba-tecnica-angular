import { Component, OnInit } from '@angular/core';
import { ItemServicesService } from 'src/app/services/item-services.service';

@Component({
  selector: 'app-delete-item',
  templateUrl: './delete-item.component.html',
  styleUrls: ['./delete-item.component.scss']
})
export class DeleteItemComponent implements OnInit {

  public data;

  constructor(private itemService: ItemServicesService) { }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.itemService.getAllService()
    .subscribe(
        (res) => { this.data = res; },
        (error) => { alert('error al obtener');}
    )
  }

  deleteItem(itemId) {
    let formData: FormData = new FormData();
    formData.append('itemId', itemId);
    this.itemService.deleteItemService(formData)
    .subscribe(
        (res) => { 
          this.getAll();
          alert('Item eliminado con exito');
      },
        (error) => {}
    )
  }

}
